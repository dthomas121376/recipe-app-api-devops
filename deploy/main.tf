terraform {
  required_version = ">= 1.0" # which means any version equal & above 0.14 like 0.15, 0.16 etc and < 1.xx
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 2.50.0"
    }
  } #commented out due to using terrafom version 0.12.21 for recipe gitlab project


  # Adding Backend as S3 for Remote State Storage
  backend "s3" {
    bucket         = "omotayio-terraform"
    key            = "training/recipe-app-api-devops-tfstate/recipe-app.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "recipe-app-api-devops-tf-state-lock"
  }
}

provider "aws" {
  region = "us-east-1"
  #version = "~> 2.50.0" #this line included since using version 0.12.21 for recipe gitlab project
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManageBy    = "Terraform"
  }
}

data "aws_region" "current" {}